import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import "./App.css";

import Home from "./pages/home/index";
import User from "./pages/user/index";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
	<React.StrictMode>
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<Home />}></Route>
				<Route path="users">
					<Route path=":userId" element={<User />} />
				</Route>
			</Routes>
		</BrowserRouter>
	</React.StrictMode>
);

import { useParams } from "react-router-dom";

const User = () => {
	const params = useParams();
	return (
		<div className="App">
			<h1>User id: {params.userId}</h1>
		</div>
	);
};

export default User;

import { Link } from "react-router-dom";
import { Table, Spin, Button } from "antd";
import { useEffect, useState } from "react";

function App() {
	const { Column } = Table;

	const [users, setUsers] = useState([]);
	const [isDisabled, setIsDisabled] = useState();
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		const fetchUsers = async () => {
			setLoading(true);
			await fetch("https://jsonplaceholder.typicode.com/users")
				.then((resp) => resp.json())
				.then((data) => {
					setUsers(data);
					setLoading(false);
				})
				.catch((err) => console.log(err));
		};
		fetchUsers();
	}, []);

	const rowSelection = {
		onChange: (selectedRowKeys, selectedRows) => {
			let [id] = selectedRowKeys;
			selectedRowKeys.length === 1
				? setIsDisabled(id)
				: setIsDisabled(null);
		},
		getCheckboxProps: (record) => {
			return {
				name: record.name,
				disabled: isDisabled === record.id,
			};
		},
	};

	const deleteUser = (id) => {
		const arr = users.filter((user) => user.id !== id);
		setUsers(arr);
		// await fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
		// 	method: "DELETE",
		// });
	};

	return (
		<div className="App">
			{loading ? (
				<Spin size={"large"} />
			) : (
				<Table
					className="table"
					dataSource={users}
					rowKey={(record) => record.id}
					rowSelection={rowSelection}
					pagination={{
						defaultPageSize: 5,
					}}
				>
					<Column
						title="ID"
						dataIndex="id"
						key="id"
						sorter={(a, b) => a.id - b.id}
					/>
					<Column
						title="Username"
						dataIndex="username"
						key="username"
					/>
					<Column
						key="action"
						render={(text, record) => (
							<Link to={`/users/${record.id}`}>Подробнее</Link>
						)}
					/>
					<Column title="E-mail" dataIndex="email" key="email" />
					<Column title="Website" dataIndex="website" key="website" />
					<Column
						key="delete"
						render={(text, record) => (
							<Button
								type="primary"
								danger
								onClick={() => deleteUser(record.id)}
							>
								Удалить
							</Button>
						)}
					/>
				</Table>
			)}
		</div>
	);
}

export default App;
